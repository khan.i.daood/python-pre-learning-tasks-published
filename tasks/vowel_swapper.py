def vowel_swapper(string):
    # ==============
    newString = string.replace('a', '4', 2)
    newString = newString.replace('A', '4', 2)
    newString = newString.replace('e', '3', 2)
    newString = newString.replace('E', '3', 2)
    newString = newString.replace('i', '!', 2)
    newString = newString.replace('I', '!', 2)
    newString = newString.replace('o', 'ooo', 2)
    newString = newString.replace('O', '000', 2)
    newString = newString.replace('u', '|_|', 2)
    newString = newString.replace('U', '|_|', 2)
    return newString
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
