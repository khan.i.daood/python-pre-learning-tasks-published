def factors(number):
    # ==============
    factorCount = 0

    for i in range(2, number):
        if (number % i == 0):
            print(i, end=", ")
            factorCount = factorCount + 1
        elif(number % i > number):
            break

    if (factorCount == 0):
        print(number, end=" you have a prime number")
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
